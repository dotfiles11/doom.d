;;; packages/gpl/gpl.el --- major mode for gpl-*- lexical-binding: t; -*-

(eval-when-compile
  (require 'rx))

(require 'xref-gpl)

(defconst gpl--keywords
  '("Add" "Aggregate" "And" "App" "AppActivate" "Application" "Array" "As"
    "Asc" "AscB" "Atn" "Attribute"
    "Beep" "Begin" "BeginTrans" "ByVal" "ByRef"
    "CBool" "CByte" "CCur"
    "CDate" "CDbl" "CInt" "CLng" "CSng" "CStr" "CVErr" "CVar" "Call"
    "Case" "ChDir" "ChDrive" "Character" "Choose" "Chr" "ChrB" "Class"
    "ClassModule" "Clipboard" "Close" "Collection" "Column" "Columns"
    "Command" "CommitTrans" "CompactDatabase" "Component" "Components"
    "Const" "Container" "Containers" "Cos" "CreateDatabase" "CreateObject"
    "CurDir" "Currency"
    "DBEngine" "DDB" "Data" "Database" "Databases"
    "Date" "DateAdd" "DateDiff" "DatePart" "DateSerial" "DateValue" "Day"
    "Debug" "Declare" "Deftype" "DeleteSetting" "Dim" "Dir" "Do"
    "DoEvents" "Domain"
    "Dynaset" "EOF" "Each" "Else" "Empty" "End" "EndProperty"
    "Enum" "Environ" "Erase" "Err" "Error" "Exit" "Exp" "Explicit" "FV" "False" "Field"
    "Fields" "FileAttr" "FileCopy" "FileDateTime" "FileLen" "Fix" "Font" "For"
    "Form" "FormTemplate" "Format" "FormatCurrency" "FormatDateTime" "FormatNumber"
    "FormatPercent" "Forms" "FreeFile" "FreeLocks" "Friend" "Function"
    "Get" "GetAllSettings" "GetAttr" "GetObject" "GetSetting" "Global" "GoSub"
    "GoTo" "Group" "Groups" "Hex" "Hour" "IIf" "IMEStatus" "IPmt" "IRR"
    "If" "Implements" "InStr" "Input" "Int" "Is" "IsArray" "IsDate"
    "IsEmpty" "IsError" "IsMissing" "IsNull" "IsNumeric" "IsObject" "Kill"
    "LBound" "LCase" "LOF" "LSet" "LTrim" "Left" "Len" "Let" "Like" "Line"
    "Load" "LoadPicture" "LoadResData" "LoadResPicture" "LoadResString" "Loc"
    "Lock" "Log" "Long" "Loop" "MDIForm" "MIRR" "Me" "MenuItems"
    "MenuLine" "Mid" "Minute" "MkDir" "Month" "MsgBox" "NPV" "NPer" "Name"
    "New" "Next" "Not" "Now" "Nothing" "Null" "Oct" "On" "Open"
    "OpenDatabase"
    "Operator" "Option" "Optional"
    "Or" "PPmt" "PV" "Parameter" "Parameters" "Partition"
    "Picture" "Pmt" "Preserve" "Print" "Printer" "Printers" "Private"
    "ProjectTemplate" "Property"
    "Properties" "Public" "Put" "QBColor" "QueryDef" "QueryDefs"
    "RSet" "RTrim" "Randomize" "Rate" "ReDim" "Recordset" "Recordsets"
    "RegisterDatabase" "Relation" "Relations" "Rem" "RepairDatabase"
    "Reset" "Resume" "Return" "Right" "RmDir" "Rnd" "Rollback" "RowBuffer"
    "SLN" "SYD" "SavePicture" "SaveSetting" "Screen" "Second" "Seek"
    "SelBookmarks" "Select" "SelectedComponents" "SendKeys" "Set"
    "SetAttr" "SetDataAccessOption" "SetDefaultWorkspace" "Sgn" "Shell"
    "Sin" "Snapshot" "Space" "Spc" "Sqr" "Static" "Step" "Stop" "Str"
    "StrComp" "StrConv" "Sub" "SubMenu" "Switch" "Tab" "Table"
    "TableDef" "TableDefs" "Tan" "Then" "Time" "TimeSerial" "TimeValue"
    "Timer" "To" "Trim" "True" "Type" "TypeName" "UBound" "UCase" "Unload"
    "Unlock" "Val" "Variant" "VarType" "Verb" "Weekday" "Wend"
    "While" "Width" "With" "Workspace" "Workspaces" "Write" "Year"
    "Module"))

(defconst gpl--types
  '("Boolean" "Byte" "Short" "Integer" "Single" "Double" "String" "Object"))
(defconst gpl--font-lock-defaults
  `(((,(rx-to-string `(: word-start
                       (or ,@gpl--keywords)
                       word-end))
      0 font-lock-keyword-face)
     (,(rx-to-string `(: word-start
                       (or ,@gpl--types)
                       word-end))
      0 font-lock-type-face)
     (,(rx-to-string `(: (or "As" "New" "Class" "Module" "As New")
                       (1+ whitespace)
                       (group (regex "[_a-zA-Z][_a-zA-Z0-9]*"))))
      1 font-lock-type-face)
     (,(rx-to-string `(: (or "Sub" "Property")
                       (1+ whitespace)
                       (group (regex "[_a-zA-Z][_a-zA-Z0-9]*"))))
      1 font-lock-function-name-face)
     ("[_a-zA-Z][_a-zA-Z0-9]*" 0 font-lock-variable-name-face))))

(defvar gpl-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?' "<" st)
    (modify-syntax-entry ?# "<" st)
    (modify-syntax-entry ?\n ">" st)
    st))

(defun gpl-indent-line ())

(defvar gpl-mode-abbrev-table nil
  "Abbreviation table used in `gpl-mode' buffers.")
(define-abbrev-table 'gpl-mode-abbrev-table
  '())

;;;###autoload
(define-derived-mode gpl-mode prog-mode "gpl"
  "Major mode for GPL"
  :abbrev-table gpl-mode-abbrev-table
  (setq font-lock-defaults gpl--font-lock-defaults)
  (setq-local comment-start "'")
  (setq-local comment-start-skip "'+[\t ]*")
  (setq-local indent-line-function #'gpl-indent-line)
  (setq-local indent-tabs-mode t)
  (setq-local xref-backend-functions #'xref-gpl-xref-backend))

(provide 'gpl)
