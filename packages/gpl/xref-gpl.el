;;; packages/gpl/xref-gpl.el -*- lexical-binding: t; -*-
;;;
;;; This is heavily inspired (copied) by xref-js2

(require 'xref)
(require 'xref-js2)
(require 'seq)

(defcustom xref-gpl-search-program 'ag
  "The backend program used for searching."
  :type 'symbol
  :group 'xref-gpl
  :options '(ag rg))

(defcustom
  xref-gpl-definitions-regexps '("Module[\\s]+\\b%s\\b"
                                 "Public[\\s]+\\b%s\\b"
                                 "Private[\\s]+\\b%s\\b"
                                 "Sub[\\s]+\\b%s\\b"
                                 "Dim[\\s]+\\b%s\\b"
                                 "Function[\\s]+\\b%s\\b")
  "List of regular expressions that match definitions of a symbol.
In each regexp string, '%s' is expanded with the search symbol."
  :type 'list
  :group 'xref-gpl)

(defcustom xref-gpl-ag-arguments '("--noheading" "--nocolor")
  "Default arguments passed to ag."
  :type 'list
  :group 'xref-gpl)

(defcustom xref-gpl-rg-arguments '("--no-heading"
                                   "--line-number" ; not activated by default on comint
                                   "--pcre2"          ; provides regexp backtracking
                                   "--ignore-case"    ; ag is case insensitive by default
                                   "--color" "never")
  "Default arguments passed to ripgrep."
  :type 'list
  :group 'xref-gpl)

(defun xref-gpl--search-ag-get-args (regexp)
  "Aggregate command line arguments to search for REGEXP using ag."
  `(,@xref-gpl-ag-arguments
    ,regexp))

(defun xref-gpl--search-rg-get-args (regexp)
  "Aggregate command line arguments to search for REGEXP using ripgrep."
  `(,@xref-gpl-rg-arguments
    ,regexp))

(defun xref-gpl--find-candidates (symbol regexp)
  (let ((default-directory (xref-js2--root-dir))
        matches)
    (with-temp-buffer
      (let* ((search-tuple (cond ;; => (prog-name . function-to-get-args)
                            ((eq xref-gpl-search-program 'rg)
                             '("rg" . xref-gpl--search-rg-get-args))
                            (t ;; (eq xref-gpl-search-program 'ag)
                             '("ag" . xref-gpl--search-ag-get-args))))
             (search-program (car search-tuple))
             (search-args    (remove nil ;; rm in case no search args given
                                     (funcall (cdr search-tuple) regexp))))
        (apply #'process-file (executable-find search-program) nil t nil search-args))

      (goto-char (point-max)) ;; NOTE maybe redundant
      (while (re-search-backward "^\\(.+\\)$" nil t)
        (push (match-string-no-properties 1) matches)))
    (seq-remove #'xref-js2--false-positive
                (seq-map (lambda (match)
                           (xref-js2--candidate symbol match))
                         matches))))

;;;###autoload
(defun xref-gpl-xref-backend ()
  "Xref-Gpl backend for Xref."
  'xref-gpl)

(cl-defmethod xref-backend-identifier-at-point ((_backend (eql xref-gpl)))
  (symbol-name (symbol-at-point)))

(cl-defmethod xref-backend-definitions ((_backend (eql xref-gpl)) symbol)
  (xref-gpl--xref-find-definitions symbol))

(defun xref-gpl--xref-find-definitions (symbol)
  (seq-map (lambda (candidate)
             (xref-js2--make-xref candidate))
           (xref-gpl--find-definitions symbol)))
(defun xref-gpl--find-definitions (symbol)
  (xref-gpl--find-candidates
   symbol
   (xref-js2--make-regexp symbol xref-gpl-definitions-regexps)))

(provide 'xref-gpl)
