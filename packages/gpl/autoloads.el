;;; autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "gpl" "gpl.el" (0 0 0 0))
;;; Generated autoloads from gpl.el

(autoload 'gpl-mode "gpl" "\
Major mode for GPL

\(fn)" t nil)

(register-definition-prefixes "gpl" '("gpl-"))

;;;***

(provide 'autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; autoloads.el ends here
