;;; autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "shield" "shield.el" (0 0 0 0))
;;; Generated autoloads from shield.el

(autoload 'shield-start-shield-services "shield" nil t nil)

(autoload 'shield-start-vendorportal-services "shield" nil t nil)

(autoload 'shield-start-assessor-portal-services "shield" nil t nil)

(autoload 'shield-start-shield-ui "shield" nil t nil)

(autoload 'shield-start-vendorroster-ui "shield" nil t nil)

(autoload 'shield-start-vendorportal-ui "shield" nil t nil)

(autoload 'shield-start-assessor-portal-ui "shield" nil t nil)

(register-definition-prefixes "shield" '("assessor-portal-" "shield-" "vendor"))

;;;***

(provide 'autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; autoloads.el ends here
