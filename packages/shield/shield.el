;;; shield.el --- Stuff specific to working on shield -*- lexical-binding: t; -*-
;; Copyright (C) 2021 Jake Shilling
;;
;; Author: Jake Shilling <https://github.com/j-shilling>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: September 27, 2021
;; Modified: September 27, 2021
;; Version: 0.0.1
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(require 'tmux+)
(require 'dash)

(defcustom shield-local-sql-connection
  (list
   (cons 'sql-product 'mysql)
   (cons 'sql-port 5432)
   (cons 'sql-server "localhost")
   (cons 'sql-database "shield_ise"))
   "")

(map-put sql-connection-alist
         'shield-local shield-local-sql-connection)

(defcustom shield-dir
  (->> (getenv "HOME")
       (expand-file-name "work")
       (expand-file-name "SHIELD"))
  "Directory where sheild repos are cloned")

(defcustom shield-brands-dir
  (->> shield-dir
       (expand-file-name ".internal")
       (expand-file-name "env")
       (expand-file-name "brands"))
  "Directory where shield brand env files are")

(defcustom shield-services-alist
  (list
   (cons 'log-file "shield-services.log")
   (cons 'start-script "shield-services")
   (cons 'tmux-session "work")
   (cons 'tmux-window "shield-services")
   (cons 'dir (expand-file-name "shield-services" shield-dir)))
  "Description of shield-services")

(defcustom vendorportal-services-alist
  (list
   (cons 'log-file "vendorportal-services.log")
   (cons 'start-script "vendorportal-services")
   (cons 'tmux-session "work")
   (cons 'tmux-window "vendorportal-services")
   (cons 'dir (expand-file-name "vendorportal-services" shield-dir)))
  "Description of vendorportal-services")

(defcustom assessor-portal-services-alist
  (list
   (cons 'log-file "assessor-portal-services.log")
   (cons 'start-script "assessor-portal-services")
   (cons 'tmux-session "work")
   (cons 'tmux-window "assessor-portal-services")
   (cons 'dir (expand-file-name "assessor-portal-services" shield-dir)))
  "Description of assessor-portal-services")

(defcustom shield-ui-alist
  (list
   (cons 'tmux-session "work")
   (cons 'tmux-window "shield-ui")
   (cons 'dir (expand-file-name "shield-ui" shield-dir)))
  "Description of shield-ui")

(defcustom vendorroster-ui-alist
  (list
   (cons 'tmux-session "work")
   (cons 'tmux-window "vendorroster-ui")
   (cons 'dir (expand-file-name "vendorroster-ui" shield-dir)))
  "Description of vendorroster-ui")

(defcustom vendorportal-ui-alist
  (list
   (cons 'tmux-session "work")
   (cons 'tmux-window "vendorportal-ui")
   (cons 'dir (expand-file-name "vendorportal-ui" shield-dir)))
  "Description of vendorportal-ui")

(defcustom assessor-portal-ui-alist
  (list
   (cons 'tmux-session "work")
   (cons 'tmux-window "assessor-portal-ui")
   (cons 'dir (expand-file-name "assessor-portal-ui" shield-dir)))
  "Description of assessor-portal-ui")

(defun shield-brands ()
  (let ((regexp "\\.env$"))
    (mapcar
     (lambda (filename) (replace-regexp-in-string regexp "" filename))
     (directory-files shield-brands-dir nil regexp))))

(defun shield-start-backend (backend-alist)
  (map-let (log-file start-script tmux-session tmux-window dir) backend-alist
    (ivy-read "Select a brand: "
              (shield-brands)
              :history 'shield-brands-history
              :action (lambda (brand)
                        (tmux+-run-in-window tmux-session
                                             tmux-window
                                             (format "'rm -f %s && mvn clean package && ./%s %s'"
                                                     log-file
                                                     start-script
                                                     brand)
                                             dir)))))

(defun shield-start-frontend (frontend-alist)
  (map-let (tmux-session tmux-window dir) frontend-alist
    (tmux+-run-in-window tmux-session
                         tmux-window
                         "npm run debug"
                         dir)))

;;;###autoload
(defun shield-start-shield-services ()
  (interactive)
  (shield-start-backend shield-services-alist))

;;;###autoload
(defun shield-start-vendorportal-services ()
  (interactive)
  (shield-start-backend vendorportal-services-alist))

;;;###autoload
(defun shield-start-assessor-portal-services ()
  (interactive)
  (shield-start-backend assessor-portal-services-alist))

;;;###autoload
(defun shield-start-shield-ui ()
  (interactive)
  (shield-start-frontend shield-ui-alist))

;;;###autoload
(defun shield-start-vendorroster-ui ()
  (interactive)
  (shield-start-frontend vendorroster-ui-alist))

;;;###autoload
(defun shield-start-vendorportal-ui ()
  (interactive)
  (shield-start-frontend vendorportal-ui-alist))

;;;###autoload
(defun shield-start-assessor-portal-ui ()
  (interactive)
  (shield-start-frontend assessor-portal-ui-alist))

(provide 'shield)
;;; shield.el ends here
