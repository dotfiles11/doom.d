;;; packages/wal/wal.el -*- lexical-binding: t; -*-
;; Copyright (C) 2021 Jake Shilling
;;
;; Author: Jake Shilling <https://github.com/j-shilling>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: September 27, 2021
;; Modified: September 27, 2021
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1") (flycheck "0.22") (dash "2.19.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

;;;###autoload
(defgroup wal nil
  "ELisp frontend for PyWal."
  :prefix "wal-"
  :group 'applications)

;;;###autoload
(defcustom wal-wal-path "wal"
  "Path of the wall executable."
  :group 'wal
  :type 'string)

;;;###autoload
(defcustom wal-finish-hook nil
  "Hook run when wal finishes."
  :group 'wal
  :type 'hook)

;;;###autoload
(defcustom wal-success-hook nil
  "Hook run when wal finishes successfully."
  :group 'wal
  :type 'hook)

(defun wal--sentinel (proc status)
  "Clean up the a wal process.

Checks whether PROC is finished running and, if not, kills the
associated buffer. Then run `wal-finish-hook' and, if STATUS is
`\"finished\\n\"', also run `wal-success-hook'."

  (unless (process-live-p proc)
    (kill-buffer (process-buffer proc))
    (run-hooks 'wal-finish-hook)
    (when (string= "finished\n" status)
      (run-hooks 'wal-success-hook))))

;;;###autoload
(defun wal-restore ()
  "Restore previous colorscheme"
  (interactive)
  (let* ((buffer (generate-new-buffer "*wal*"))
         (args '("-R")))
    (when (require 'exec-path-from-shell nil t)
      (exec-path-from-shell-initialize))
    (make-process :name "wal"
                  :buffer buffer
                  :sentinel #'wal--sentinel
                  :command `(,wal-wal-path ,@args))))

(provide 'wal)
;;; wal.el ends here
