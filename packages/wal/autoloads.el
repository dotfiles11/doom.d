;;; autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "wal" "wal.el" (0 0 0 0))
;;; Generated autoloads from wal.el

(let ((loads (get 'wal 'custom-loads))) (if (member '"wal" loads) nil (put 'wal 'custom-loads (cons '"wal" loads))))

(defvar wal-wal-path "wal" "\
Path of the wall executable.")

(custom-autoload 'wal-wal-path "wal" t)

(defvar wal-finish-hook nil "\
Hook run when wal finishes.")

(custom-autoload 'wal-finish-hook "wal" t)

(defvar wal-success-hook nil "\
Hook run when wal finishes successfully.")

(custom-autoload 'wal-success-hook "wal" t)

(autoload 'wal-restore "wal" "\
Restore previous colorscheme." t nil)

(register-definition-prefixes "wal" '("wal--sentinel"))

;;;***

(provide 'autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; autoloads.el ends here
