;;; autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "tmux+" "tmux+.el" (0 0 0 0))
;;; Generated autoloads from tmux+.el

(autoload 'tmux+-get-session-by-name "tmux+" "\


\(fn NAME)" nil nil)

(autoload 'tmux+-ensure-session "tmux+" "\


\(fn NAME)" nil nil)

(autoload 'tmux+-get-window-by-name "tmux+" "\


\(fn NAME &optional SESSION)" nil nil)

(autoload 'tmux+-run-in-window "tmux+" "\


\(fn SESSION-NAME WINDOW-NAME COMMAND &optional DIRECTORY)" nil nil)

(register-definition-prefixes "tmux+" '("tmux+"))

;;;***

(provide 'autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; autoloads.el ends here
