;;; tmux+.el --- Extend doom tmux module -*- lexical-binding: t; -*-
;; Copyright (C) 2021 Jake Shilling
;;
;; Author: Jake Shilling <https://github.com/j-shilling>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: September 27, 2021
;; Modified: September 27, 2021
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1") (flycheck "0.22") (dash "2.19.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:
(require 'cl-lib)
(require 'seq)
(require 'subr-x)
(require 'projectile)
(require 'dash)

(defun tmux+-list-sessions ()
  (if (symbol-function '+tmux-list-sessions)
      (condition-case _e
          (+tmux-list-sessions)
        ('error nil))
    (error "Doom's tools: tmux module is not loaded")))

(defun tmux+-list-windows (&optional session)
  (if (symbol-function '+tmux-list-windows)
      (condition-case _e
          (+tmux-list-windows session)
        ('error nil))
    (error "Doom's tools: tmux module is not loaded")))

(defun tmux+ (command &rest args)
  (if (symbol-function 'tmux+)
      (+tmux command args)
    (error "Doom's tools: tmux module is not loaded")))

(defun tmux+-find-by-name (name list)
  (--first (map-let ((:name session-name)) (cdr it)
             (string= session-name name))
           list))

;;;###autoload
(defun tmux+-get-session-by-name (name)
  (tmux+-find-by-name name (tmux+-list-sessions)))

;;;###autoload
(defun tmux+-ensure-session (name)
  (if-let ((session (tmux+-get-session-by-name name)))
      session
    (progn
      (tmux+ (format "new -s %s -d" name))
      (tmux+-get-session-by-name name))))

;;;###autoload
(defun tmux+-get-window-by-name (name &optional session)
  (tmux+-find-by-name name (tmux+-list-windows session)))

;;;###autoload
(defun tmux+-run-in-window (session-name window-name command &optional directory)
  (let ((session (tmux+-ensure-session session-name))
        (dir (or directory
                 (projectile-project-root)
                 default-directory)))
    (let ((window (tmux+-get-window-by-name window-name session)))
      (when (or (not window)
                (yes-or-no-p "A window with that name already exists. Ok to kill it?"))
       (tmux+ (format "new-window -c '%s' -n %s -t '%s' -d -k %s"
                      dir window-name (if window
                                          (format "%s:%s"
                                                  (plist-get (cdr window) :session-id)
                                                  (car window))
                                        session-name)
                      command))))))

;;;###autoload
(cl-defun tmux+-capture-pane
    (&key buffer-name end-line start-line target-pane
          print alternate-screen quiet escape escape-all
          join-lines capture-incomplete)
  (let ((short-args (->> `((,alternate-screen . "a")
                           (,escape . "e")
                           (,print . "p")
                           (,capture-incomplete . "P")
                           (,quiet . "q")
                           (,escape-all . "C")
                           (,join-lines . "J"))
                         (--filter (car it))
                         (--map (cdr it))
                         (apply #'concat)))
        (long-args (->> `((,buffer-name . "b")
                          (,end-line . "E")
                          (,start-line . "S")
                          (,target-pane . "t"))
                        (--filter (car it))
                        (--map (format "-%s %s" (cdr it) (car it))))))
    (let ((args-string (-> (seq-concatenate 'list
                                            (unless
                                                (string= "" short-args)
                                              (list (concat "-" short-args)))
                                            long-args)
                           (string-join " "))))
      (tmux+ (format "capture-pane %s" args-string)))))

(defmacro tmux+-register-command
    (command &optional args flags)
  (let ((fn-name (intern (format "tmux+-%s" command)))
        (keys (--map (intern (plist-get it :key)) flags))
        (no-arg-flags (--filter (not (plist-get it :has-arg)) flags))
        (arg-flags (--filter (plist-get it :has-arg) flags)))
    (let ((args-list (seq-concatenate 'list
                                      args
                                      (when keys
                                        (cons '&key keys)))))
      `(cl-defun ,fn-name ,args-list
         (let ((short-args (->> (list
                                 ,@(--map
                                    (list
                                     'cons
                                     (intern (plist-get it :key))
                                     (plist-get it :flag))
                                    no-arg-flags))
                                (--filter (car it))
                                (--map (cdr it))
                                (apply #'concat)))
               (long-args (->> (list
                                ,@(--map
                                    (list
                                     'cons
                                     (intern (plist-get it :key))
                                     (plist-get it :flag))
                                    arg-flags)
                                (--filter (car it))
                                (--map (format "-%s %s" (cdr it) (car it)))))))
           (let ((args (seq-concatenate 'list
                                        (unless (string= "" short-args)
                                          (concat "-" short-args))
                                        long-args)))
             args))))))

(provide 'tmux+)
;;; tmux+.el ends here
