;;; autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "csharp+" "csharp+.el" (0 0 0 0))
;;; Generated autoloads from csharp+.el

(autoload '+csharp-current-file-is-interface-p "csharp+" "\
Return non-nil if the current file is an interface.

\(fn _)" nil nil)

(autoload '+csharp-current-file-is-class-p "csharp+" "\


\(fn _)" nil nil)

(autoload '+csharp-current-class "csharp+" "\
Get the class name for the current file." nil nil)

;;;***

(provide 'autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; autoloads.el ends here
