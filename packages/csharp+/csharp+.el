;;; packages/csharp+/csharp+.el -*- lexical-binding: t; -*-

;;;###autoload
(defun +csharp-current-file-is-interface-p (_)
  "Return non-nil if the current file is an interface."
  (string-prefix-p "I"
                   (file-name-base
                    (buffer-file-name))))

;;;###autoload
(defun +csharp-current-file-is-class-p (_)
  (not (+csharp-current-file-is-interface-p _)))

;;;###autoload
(defun +csharp-current-class ()
  "Get the class name for the current file."
  (unless (eq major-mode 'csharp-mode)
    (user-error "Not in a csharp-mode buffer"))
  (unless buffer-file-name
    (user-error "This buffer has no filepath; cannot guess its class name"))
  (or (file-name-sans-extension (file-name-base (buffer-file-name)))
      "ClassName"))
