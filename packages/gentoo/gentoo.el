;;; packages/gentoo/gentoo.el -*- lexical-binding: t; -*-

(require 'dash)

(defgroup gentoo nil
  "Gentoo")

(defcustom gentoo-world-file
  "/var/lib/portage/world"
  "Location of the world file on this system"
  :type '(file :must-match t)
  :group 'gentoo)

(defun gentoo--has-reverse-dependencies-p (x)
  "Returns `t' if the atom X has reverse dependencies."
  (with-temp-buffer
    (call-process "qdepends" nil t nil "-Q" x)
    (let ((output (buffer-string)))
      (if (string-match-p "no matches found for your query" output)
          nil
        (< 0 (seq-length output))))))

(defun gentoo--safe-to-deselect-p (x)
  "Returns `t' if atom X can be depclean"
  (with-temp-buffer
    (call-process "emerge" nil t nil "-p" "--quiet" "--depclean" x)
    (= 0 (seq-length (buffer-string)))))

(defun gentoo--should-deselect (x)
  (and (gentoo--has-reverse-dependencies-p x)
       (gentoo--safe-to-deselect-p x)))

(defun gentoo--world-set ()
  (with-temp-buffer
    (insert-file-contents gentoo-world-file)
    (split-string (buffer-string) "\n" t)))

(defun gentoo--unnecessary-selected-atoms ()
  (--filter (gentoo--should-deselect it)
            (gentoo--world-set)))


(provide 'gentoo)
