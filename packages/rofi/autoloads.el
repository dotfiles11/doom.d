;;; autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:


;;;### (autoloads nil "rofi" "rofi.el" (0 0 0 0))
;;; Generated autoloads from rofi.el

(autoload 'rofi-run "rofi" nil t nil)

(register-definition-prefixes "rofi" '("rofi--read"))

;;;***

(provide 'autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; autoloads.el ends here
