;;; rofi.el --- Interact with Rofi -*- lexical-binding: t; -*-
;; Copyright (C) 2021 Jake Shilling
;;
;; Author: Jake Shilling <https://github.com/j-shilling>
;; Maintainer: Jake Shilling <shilling.jake@gmail.com>
;; Created: September 27, 2021
;; Modified: September 27, 2021
;; Version: 0.0.1
;; Package-Requires: ((emacs "25.1") (dash "2.19.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Description
;;
;;; Code:

(defun rofi--read (prompt candidates)
  (let ((candidate-strings (--map
                            (if (stringp it)
                                it
                              (prin1-to-string it))
                            candidates)))
    (with-temp-buffer
      (insert (string-join candidate-strings "\n"))
      (call-process-region (point-min) (point-max)
                           "rofi" t t nil "-p" prompt "-dmenu")
      (string-trim (buffer-string)))))

;;;###autoload
(defun rofi-run ()
  (interactive)
  (call-process "rofi" nil nil nil "-show" "run"))

;;;###autoload
(defun rofi-window ()
  (interactive)
  (call-process "rofi" nil nil nil "-show" "window"))

(provide 'rofi)
;;; rofi.el ends here
